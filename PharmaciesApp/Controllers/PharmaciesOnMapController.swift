//
//  PharmaciesOnMapController.swift
//  PharmaciesApp
//
//  Created by a on 23/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class PharmaciesOnMapController: UIViewController , MKMapViewDelegate, NavigationBarHeader{
    
    var pharmacies = [Pharmacy]()
    let locationManager = CLLocationManager()
    
    var pharmaciesOnMapView = PharmaciesOnMapView()
    var currentUserLocation: CLLocationCoordinate2D?

    override func loadView() {
        view = pharmaciesOnMapView
    }
    
    fileprivate func centerMap(_ mapView: MKMapView) {
        let center = currentUserLocation ?? CLLocationCoordinate2D()
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: center, span: span)
        mapView.setRegion(region, animated: false)
    }
    
    func getCurrentLocation(){
        let getLocationGroup = DispatchGroup()
        getLocationGroup.enter()
        
        guard let location = self.locationManager.location?.coordinate else {
            getLocationGroup.leave()
            return
        }
        self.currentUserLocation = location
        
        getLocationGroup.leave()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showNavigationBarHeader(headerImageView: nil)
        
        self.navigationItem.title = LocalizedString(key: "Map")
        getCurrentLocation()

        pharmaciesOnMapView.mapView.frame = view.frame
        pharmaciesOnMapView.mapView.delegate = self
        pharmaciesOnMapView.mapView.showsUserLocation = true
        centerMap(pharmaciesOnMapView.mapView)
        
        for location in pharmacies {
            let annotation = MKPointAnnotation()
            annotation.title = location.name
            guard let latitude = location.Latitude else { return }
            guard let longitude = location.longitude else { return }
            annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            pharmaciesOnMapView.mapView.addAnnotation(annotation)
        }
    }
}

extension PharmaciesOnMapController: CLLocationManagerDelegate{
}
