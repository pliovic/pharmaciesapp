//
//  AvailablePillsController.swift
//  PharmaciesApp
//
//  Created by a on 29/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit

class AvailablePillsController: UIViewController {
    
    var pills = Set<Drug>()
    
    let tableView = UITableView()
    
    func setupTableView(){
        tableView.frame = view.frame
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        view.addSubview(tableView)
        tableView.register(AbailablePillsTableViewCell.self, forCellReuseIdentifier: "Cell")
        
        tableView.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(view)
            make.left.right.equalTo(view)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupTableView()
    }
    
}

extension AvailablePillsController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pills.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AbailablePillsTableViewCell
        
        cell.drugNameView.text = pills[pills.index(pills.startIndex, offsetBy: indexPath.row)].name
        cell.doseView.text = String(pills[pills.index(pills.startIndex, offsetBy: indexPath.row)].dose ?? 0)
        
        if(pills[pills.index(pills.startIndex, offsetBy: indexPath.row)].contain ?? false){
            cell.backgroundColor = .systemGreen
        }
        else{
            cell.backgroundColor = .systemRed
        }
        return cell
    }
}
