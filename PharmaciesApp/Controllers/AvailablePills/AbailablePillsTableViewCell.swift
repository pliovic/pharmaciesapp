//
//  AbailablePillsTableViewCell.swift
//  PharmaciesApp
//
//  Created by a on 29/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit

class AbailablePillsTableViewCell: UITableViewCell {
    
    lazy var drugNameView: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"HelveticaNeue", size: 18.0)
        label.textColor = .label
        return label
    }()
    
    private func drugNameViewAutoLayout(){
        drugNameView.snp.makeConstraints{(make) -> Void in
            make.centerY.equalTo(self)
            make.left.equalTo(self.snp.left).offset(20)
        }
    }
    
    lazy var doseView: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"HelveticaNeue", size: 18.0)
        label.textColor = .label
        return label
    }()
    
    private func doseViewViewAutoLayout(){
        doseView.snp.makeConstraints{(make) -> Void in
            make.centerY.equalTo(self)
            make.right.equalTo(self.snp.right).offset(-20)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.addSubview(drugNameView)
        self.addSubview(doseView)
        drugNameViewAutoLayout()
        doseViewViewAutoLayout()
    }
}
