//
//  BrowsePharmaciesController.swift
//  PharmaciesApp
//
//  Created by a on 23/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit
import SnapKit
import Firebase
import SwiftKeychainWrapper
import Firebase
import CoreLocation

class BrowsePharmaciesController: UIViewController, Spinnable, NavigationBarHeader{
    
    var browsePharmaciesView = BrowsePharmaceisView()
    var pharmacies = [Pharmacy]()
    
    let locationManager = CLLocationManager()
    
    var currentUserLocation: CLLocationCoordinate2D?
    lazy var ref: Database = Database.database()
    var pharmaciesRef: DatabaseReference?
    
    
    private let tableView:UITableView={
        let tableView = UITableView()
        tableView.backgroundColor = .systemBackground
        return tableView;
    }()
    
    private func setTableView(){
        tableView.frame = view.frame
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(BrowseItemTableViewCell.self, forCellReuseIdentifier: "Cell")
        view.addSubview(tableView)
    }
    
    private func tableViewAutoLayout(){
        tableView.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(view)
            make.left.right.equalTo(view)
        }
    }
    
    func getDistance(dict: [String: Any]) -> Double {
        let pharmacysCoordinate = CLLocationCoordinate2D(
            latitude: dict["latitude"] as? Double ?? 0,
            longitude: dict["longitude"] as? Double ?? 0)
        
        return (self.currentUserLocation?.distanceTo(coordinate: pharmacysCoordinate) ?? 0)/1000
        
    }
    
    func fetchPharmacies(){
        pharmaciesRef?.observe(.value, with: { snapshot in
            for pharmacy in snapshot.children.allObjects as! [DataSnapshot]{
                
                var pharmacyModel = Pharmacy(id: pharmacy.key, dictionary: pharmacy.value as! [String : Any])
                
                pharmacyModel.distance = self.getDistance(dict: pharmacy.value as! [String : Any])
                self.pharmacies.append(pharmacyModel)
            }
            
            self.tableView.reloadData()
            self.hideActivityIndicator()
            
        }, withCancel: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        showImage(false)
        
        pharmaciesRef?.removeAllObservers()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showImage(true)
    }
    
    private func showImage(_ show: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.browsePharmaciesView.mapImageView.alpha = show ? 1.0 : 0.0
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        _ = tapGestureRecognizer.view as! UIImageView
        let loadVC = PharmaciesOnMapController()
        loadVC.pharmacies = self.pharmacies
        self.navigationController?.pushViewController(loadVC, animated: true)
    }
    
    func setMapImageView(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        showNavigationBarHeader(headerImageView: browsePharmaciesView.mapImageView)
        browsePharmaciesView.mapImageView.isUserInteractionEnabled = true
        browsePharmaciesView.mapImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    func reloadView() {
        let parent = view.superview
        view.removeFromSuperview()
        view = nil
        parent?.addSubview(view)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pharmaciesRef = ref.reference(withPath: "pharmacies")
        setMapImageView()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    print("No access")
                case .authorizedAlways, .authorizedWhenInUse:
                    pharmacies.removeAll()
                    locationManager.startUpdatingLocation()
                    setTableView()
                    fetchPharmacies()
                @unknown default:
                break
            }
            } else {
                print("Location services are not enabled")
        }
        
        self.navigationItem.title = LocalizedString(key: "Pharmacies")
        view.BackgroundColor()
        
        showActivityIndicator()
    }
}

extension BrowsePharmaciesController: UITableViewDelegate, UITableViewDataSource, BrowsePharmaceisDelegate, CLLocationManagerDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pharmacies.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BrowseItemTableViewCell
        
        cell.pharmacyNameView.text = self.pharmacies[indexPath.row].name
        cell.pharmacyPhoneView.text = self.pharmacies[indexPath.row].phone
        let pharmacysCoordinate = CLLocationCoordinate2D(latitude: self.pharmacies[indexPath.row].Latitude ?? 0, longitude: self.pharmacies[indexPath.row].longitude ?? 0)
        
        let distanceFromCurrentUserLocationToPharmacy = (self.currentUserLocation?.distanceTo(coordinate: pharmacysCoordinate) ?? 0)/1000
        cell.pharmacyDistanceView.text = String(format: "%.2f km", distanceFromCurrentUserLocationToPharmacy)

//        cell.pharmacyDistanceView.text = String(format: "%.2f km", self.pharmacies[indexPath.row].distance ?? 0)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let loadVC = PharmacyDetailViewController()
        loadVC.pharmacy = self.pharmacies[indexPath.row]
        self.navigationController?.pushViewController(loadVC, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        self.currentUserLocation = locValue
        self.tableView.reloadData()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard let location = manager.location?.coordinate else {
            return
        }
        self.currentUserLocation = location
        self.reloadView()
    }
}

extension CLLocationCoordinate2D {
    func distanceTo(coordinate: CLLocationCoordinate2D) -> CLLocationDistance {
        let thisLocation = CLLocation(latitude: self.latitude, longitude: self.longitude)
        let otherLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        return thisLocation.distance(from: otherLocation)
    }
}
