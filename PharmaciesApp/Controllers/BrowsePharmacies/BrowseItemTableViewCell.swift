//
//  BrowseItemTableViewCell.swift
//  PharmaciesApp
//
//  Created by a on 25/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit

class BrowseItemTableViewCell: UITableViewCell {
    
    lazy var pharmacyNameView: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"HelveticaNeue", size: 18.0)
        label.textColor = .label
        return label
    }()
    
    private func pharmacyNameViewAutoLayout(){
        pharmacyNameView.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(self.snp.top).offset(8)
            make.leftMargin.equalTo(10)
        }
    }
    
    lazy var pharmacyPhoneView: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"HelveticaNeue", size: 16.0)
        label.textColor = .label
        return label
    }()
    
    lazy var pharmacyDistanceView: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"HelveticaNeue", size: 16.0)
        label.textColor = .label
        return label
    }()
    
    private func pharmacyDistanceViewAutoLayout(){
        pharmacyDistanceView.snp.makeConstraints{(make) -> Void in
            make.centerY.equalTo(self)
            make.rightMargin.equalTo(-10)
        }
    }
    
    lazy var phoneIconView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: "Phone")
        return image
    }()
    
    private func pharmacyPhoneViewAutoLayout(){
        phoneIconView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.snp.bottom).offset(-8)
            make.width.height.equalTo(20)
            make.leftMargin.equalTo(10)
        }
        pharmacyPhoneView.snp.makeConstraints{(make) -> Void in
            make.bottom.equalTo(self.snp.bottom).offset(-8)
            make.left.equalTo(phoneIconView.snp.right).offset(10)
        }
    }
    
    lazy var pharmacyLocation: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"HelveticaNeue", size: 20.0)
        return label
    }()
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.BackgroundColor()
        self.addSubview(pharmacyNameView)
        self.addSubview(pharmacyPhoneView)
        self.addSubview(phoneIconView)
        self.addSubview(pharmacyDistanceView)

        pharmacyNameViewAutoLayout()
        pharmacyPhoneViewAutoLayout()
        pharmacyDistanceViewAutoLayout()
    }
}
