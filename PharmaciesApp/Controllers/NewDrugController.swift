//
//  NewDrugController.swift
//  PharmaciesApp
//
//  Created by a on 23/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit
import Firebase
import SwiftKeychainWrapper
import Firebase

class NewDrugViewController: UIViewController {
    
    var newDrugView = NewDrugView()
    
    func showAlert(for alert: String) {
        let alertController = UIAlertController(title: nil, message: alert, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: LocalizedString(key: "OK"), style: .default, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    override func loadView() {
        view = newDrugView;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newDrugView.delegate = self
    }
}

extension NewDrugViewController: NewDrugViewDelegate{
    func didTapSubmitNewUsersDrugButton() {
        let userID = Auth.auth().currentUser?.uid
        
        do {
            let drugName = try self.newDrugView.drugNameTxtField.validatedText(validationType: .requiredField(field: "Drug name"))
            let drugDose = try self.newDrugView.drugDoseTxtField.validatedText(validationType: ValidatorType.doseField)
            
            let drugsRef = Database.database().reference(withPath: "drugs")
            let insertDrug = drugsRef.childByAutoId()
            insertDrug.setValue(["name": drugName, "dose": Double(drugDose) ?? 0])
            
            let userDrugsRef = Database.database().reference(withPath: "users/\(userID ?? "")/user-drugs")
            userDrugsRef.child(insertDrug.key ?? "").setValue(true)
            
            navigationController?.popViewController(animated: true)
            
        } catch(let error) {
            showAlert(for: (error as! ValidationError).message)
        }
        
    }
}
