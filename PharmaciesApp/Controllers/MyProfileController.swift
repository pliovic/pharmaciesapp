//
//  MyProfileController.swift
//  PharmaciesApp
//
//  Created by a on 23/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit
import SnapKit
import Firebase
import SwiftKeychainWrapper
import Firebase
import GoogleSignIn

class MyProfileController: UIViewController, Spinnable, NavigationBarHeader {
    
    var drugViewModel = [Drug]()
    
    lazy var ref: Database = Database.database()
    var profileDataRef: DatabaseReference?
    
    var user: User?{
        didSet{
            updateView()
        }
    }
    
    private let pillsView = UIImageView(image: UIImage(named: "Pills"))
    
    func base64Convert(base64String: String?) -> UIImage{
        if (base64String?.isEmpty)! {
            return #imageLiteral(resourceName: "no_image_found")
        }else {
            let temp = base64String?.components(separatedBy: ",")
            let dataDecoded : Data = Data(base64Encoded: temp![1], options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            return decodedimage!
        }
    }
    
    func updateView(){
        self.nameLabel.text = user?.name
        guard let url = URL(string:user?.photo ?? "") else { return }
        if let data = try? Data(contentsOf: url)
        {
            let image: UIImage = UIImage(data: data) ?? UIImage()
            self.profileImage.image = image
        }
        self.emailLabel.text = user?.email
    }
    
    lazy var profileImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = image.frame.size.width / 2
        return image
    }()
    
    private func profileImageAutoLayout(){
        profileImage.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(view).offset(30)
            make.height.equalTo(140)
        }
    }
    
    private let logoutButton:UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .systemRed
        button.setTitle(LocalizedString(key: "logout"), for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.tintColor = .white
        button.addTarget(self, action: #selector(didTapLogoutButton), for: .touchUpInside)
        return button
    }()
    func logoutButtonAutoLayout(){
        logoutButton.snp.makeConstraints { (make) in
            make.bottom.equalTo(view.snp.bottom).offset(-100)
            make.left.equalTo(view.snp.left).offset(50)
            make.right.equalTo(view.snp.right).offset(-50)
            make.height.equalTo(50)
        }
    }
    
    var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"HelveticaNeue", size: 18)
        label.textColor = .label
        return label
    }()
    
    func nameLabelAutoLAyout(){
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(profileImage.snp.right).offset(40)
        }
    }
    
    var emailLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"HelveticaNeue", size: 18)
        label.textColor = .label
        return label
    }()
    
    func emailLabelAutoLAyout(){
        emailLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.bottom).offset(20)
            make.left.equalTo(profileImage.snp.right).offset(40)
        }
    }
    
    @IBAction func didTapLogoutButton(sender: AnyObject){
        let firebaseAuth = Auth.auth()
        do{
            
            try firebaseAuth.signOut()
            let _ : Bool = KeychainWrapper.standard.removeObject(forKey: "UID")
            
            reloadView()
            
        }catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        _ = tapGestureRecognizer.view as! UIImageView
        self.navigationController?.pushViewController(DrugListController(), animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        showImage(false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showImage(true)
    }
    
    private func showImage(_ show: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.pillsView.alpha = show ? 1.0 : 0.0
        }
    }
    
    func fetchProfileData(){
        profileDataRef?.observe(.value, with: { snapshot in
            self.user = User(id: snapshot.key , dictionary: snapshot.value as? [String : Any] ?? [:])
            self.updateView()
            self.hideActivityIndicator()
        })
    }
    
    func googleLogin(){
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
        let signInButton = GIDSignInButton()
        pillsView.isHidden = true
        
        view.addSubview(signInButton)
        signInButton.snp.makeConstraints{(make) -> Void in
            make.bottom.equalTo(view.snp.top).offset(250)
        }
    }
    
    func userIsLoggedIn(){
        let userID = Auth.auth().currentUser?.uid
        profileDataRef = ref.reference(withPath: "users/\(userID ?? "")")
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        pillsView.isUserInteractionEnabled = true
        pillsView.addGestureRecognizer(tapGestureRecognizer)
        
        pillsView.isHidden = false
        
        view.addSubview(logoutButton)
        logoutButtonAutoLayout()
        
        view.addSubview(profileImage)
        profileImageAutoLayout()
        
        view.addSubview(nameLabel)
        nameLabelAutoLAyout()
        
        view.addSubview(emailLabel)
        emailLabelAutoLAyout()
        
        fetchProfileData()
        showActivityIndicator()
    }
    
    public func firebaseLogin(_ credential: AuthCredential) {
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error {
                print(error)
                return
            }
            
            Auth.auth().addStateDidChangeListener { (auth, user) in
                if let user = user {
                    let _: Bool = KeychainWrapper.standard.set(user.uid, forKey: "UID")
                    let childRef = Database.database().reference(withPath: "users")
                    
                    childRef.observe(.value, with: { snapshot in
                        var insert = false
                        for child in snapshot.children.allObjects as? [DataSnapshot] ?? []{
                            if(user.uid == child.key){
                                insert = true
                                break
                            }
                        }
                        if(!insert){
                            childRef.child(user.uid).setValue(["uid": user.uid,
                                                               "email": user.email,
                                                               "name": user.displayName,
                                                               "phoneNumber": user.phoneNumber,
                                                               "photo": user.photoURL?.absoluteString])
                        }
                        
                        
                    }, withCancel: nil)
                }
                self.reloadView()
            }
        }
    }
    
    func reloadView() {
        let parent = view.superview
        view.removeFromSuperview()
        view = nil
        parent?.addSubview(view)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = LocalizedString(key: "myProfile")
        showNavigationBarHeader(headerImageView: pillsView)
        view.BackgroundColor()
        
        let retrievedPassword: String? = KeychainWrapper.standard.string(forKey: "UID")
        
        switch retrievedPassword {
        case .none:
            googleLogin()
        default:
            userIsLoggedIn()
        }
    }
}
