//
//  DrugListController.swift
//  PharmaciesApp
//
//  Created by a on 23/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import Firebase

class DrugListController: UIViewController, NavigationBarHeader, Spinnable {
    var drugs = [Drug]()
    
    lazy var ref: Database = Database.database()
    var userDrugsRef: DatabaseReference?
    
    let tableView:UITableView={
        let tableView = UITableView()
        tableView.backgroundColor = .systemBackground
        return tableView;
    }()
    
    private func setTableView(){
        tableView.frame = view.frame
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(DrugTableViewCell.self, forCellReuseIdentifier: "Cell")
        view.addSubview(tableView)
    }
    
    @objc func addNewPill(){
        navigationController?.pushViewController(NewDrugViewController(), animated: true)
    }
    
    func fetchDrugs(){
        userDrugsRef?.observeSingleEvent(of: .value, with: { snapshot in
            for userDrug in snapshot.children.allObjects as? [DataSnapshot] ?? []{
                let drugsRef = Database.database().reference(withPath: "drugs")
                drugsRef.observeSingleEvent(of: .value) { (dataSnaphot) in
                    for drug in dataSnaphot.children.allObjects as? [DataSnapshot] ?? []{
                        if(userDrug.key == drug.key){
                            let drugModel = Drug(id: drug.key, dictionary: drug.value as? [String: Any] ?? [:])
                            self.drugs.append(drugModel)
                        }
                    }
                    self.tableView.reloadData()
                    self.hideActivityIndicator()
                }
            }
            
        }, withCancel: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        drugs.removeAll()
        fetchDrugs()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userID = Auth.auth().currentUser?.uid
        userDrugsRef = ref.reference(withPath: "users/\(userID ?? "")/user-drugs")
        self.navigationItem.title = LocalizedString(key: "myPills")
        
        showNavigationBarHeader(headerImageView: nil)
        setTableView()
        
        let addItemButton = UIBarButtonItem(title: LocalizedString(key: "add"), style: .done, target: self, action: #selector(addNewPill))
        self.navigationItem.rightBarButtonItem  = addItemButton
        
        view.BackgroundColor()
        showActivityIndicator()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        userDrugsRef?.removeAllObservers()
    }
}

extension DrugListController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.drugs.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? DrugTableViewCell
        
        cell?.pharmacyNameView.text = self.drugs[indexPath.row].name
        cell?.drugDoseView.text = String(self.drugs[indexPath.row].dose ?? 0)
        return cell ?? DrugTableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let userID = Auth.auth().currentUser?.uid
            guard let drugId = self.drugs[indexPath.row].id else {return}
            
            let drugsUsersRef = Database.database().reference(withPath: "users/\(userID ?? "")/user-drugs/").child(drugId)
            drugsUsersRef.removeValue{error, _ in print(error as Any)}
            self.drugs.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        let loadVC = PharmacyDetailViewController()
    //        loadVC.pharmaciesViewModel = self.pharmaciesViewModel[indexPath.row]
    //        self.navigationController?.pushViewController(loadVC, animated: true)
    //    }
}
