//
//  DrugTableViewCell.swift
//  PharmaciesApp
//
//  Created by a on 23/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit

class DrugTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    lazy var pharmacyNameView: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"HelveticaNeue", size: 16.0)
        label.textColor = .label
        return label
    }()
    
    private func pharmacyNameViewAutoLayout(){
        pharmacyNameView.snp.makeConstraints{(make) -> Void in
            make.centerY.equalTo(self)
            make.left.equalTo(self.snp.left).offset(15)
        }
    }
    lazy var drugDoseView: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"HelveticaNeue", size: 16.0)
        label.textColor = .label
        return label
    }()
    
    private func drugDoseViewAutoLayout(){
        drugDoseView.snp.makeConstraints{(make) -> Void in
            make.centerY.equalTo(self)
            make.right.equalTo(self.snp.right).offset(-30)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.BackgroundColor()
        self.addSubview(pharmacyNameView)
        self.addSubview(drugDoseView)
        pharmacyNameViewAutoLayout()
        drugDoseViewAutoLayout()
    }
    
}
