//
//  TabBarController.swift
//  PharmaciesApp
//
//  Created by a on 23/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    func resizedImage(url: String, for size: CGSize) -> UIImage? {
        guard let image = UIImage(named: url) else {
            return nil
        }

        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { (context) in
            image.draw(in: CGRect(origin: .zero, size: size))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.barTintColor = .systemBackground
        UITabBar.appearance().tintColor = UIColor.systemGreen
        tabBar.isTranslucent = true
        let browsePharmaciesController = UINavigationController(rootViewController: BrowsePharmaciesController())
        let myProfileController = UINavigationController(rootViewController: MyProfileController())
        
        viewControllers = [browsePharmaciesController, myProfileController]
        
        self.selectedViewController = myProfileController
        
        let pharmacyImage = resizedImage(url: "Pharmacy", for: CGSize(width: 25, height: 25))
        let profileImage = resizedImage(url: "Profile", for: CGSize(width: 25, height: 25))
        
        browsePharmaciesController.tabBarItem = UITabBarItem(title: LocalizedString(key: "browsePharmacy"), image: pharmacyImage, tag: 0)
        myProfileController.tabBarItem = UITabBarItem(title: LocalizedString(key: "myProfile"), image: profileImage, tag: 1)
    }
}
