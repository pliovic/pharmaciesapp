//
//  PharmacyDetailController.swift
//  PharmaciesApp
//
//  Created by a on 23/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit
import MapKit
import SnapKit
import Firebase

class PharmacyDetailViewController: UIViewController, MKMapViewDelegate, NavigationBarHeader, Spinnable {
    
    var pharmacyDetailsView = PharmacyDetailView()
    var pills: Set<Drug> = []
    var pharmacyDrugs: [Drug] = []
    var userDrugs: [Drug] = []
    var pharmacy = Pharmacy()
    
    func updateView(){
        pharmacyDetailsView.addressLabel.text = pharmacy.address
        pharmacyDetailsView.phoneNumberButton.setTitle(pharmacy.phone, for: .normal)
        showPharmacyOnMap(pharmacyDetailsView.mapView)
    }
    let annotation = MKPointAnnotation()
    
    func fetchUsersDrugsData(){
        let userID = Auth.auth().currentUser?.uid
        let userDrugsRef = Database.database().reference(withPath: "users/\(userID ?? "")/user-drugs")
        userDrugsRef.observeSingleEvent(of: .value, with: { snapshot in
            for userDrug in snapshot.children.allObjects as? [DataSnapshot] ?? []{
                let drugsRef = Database.database().reference(withPath: "drugs")
                drugsRef.observeSingleEvent(of: .value) { (dataSnaphot) in
                    for drug in dataSnaphot.children.allObjects as? [DataSnapshot] ?? []{
                        if(userDrug.key == drug.key){
                            
                            let drugModel = Drug(id: drug.key, dictionary: drug.value as? [String: Any] ?? [:])
                            
                            self.userDrugs.append(drugModel)
                        }
                    }
                    DispatchQueue.global(qos: .background).async {
                        DispatchQueue.main.async {
                            for userDrug in self.userDrugs{
                                
                                let contained = self.pharmacyDrugs.contains(userDrug)
                                var drugContain = Drug()
                                drugContain.id = userDrug.id
                                drugContain.name = userDrug.name
                                drugContain.dose = userDrug.dose
                                drugContain.contain = contained
                                
                                self.pills.insert(drugContain)
                                self.hideActivityIndicator()
                            }
                            
                        }
                    }
                }
            }
            
        }, withCancel: nil)
    }
    
    func fetchPharmacysDrugsData(){
        guard let pharmacyId = pharmacy.id else { return }
        let pharmacyDrugsRef = Database.database().reference(withPath: "pharmacies/\(pharmacyId)/pharmacy-drugs")
        pharmacyDrugsRef.observeSingleEvent(of: .value) { snapshot in
            for pharmacyDrug in snapshot.children.allObjects as? [DataSnapshot] ?? []{
                let drugsRef = Database.database().reference(withPath: "drugs")
                drugsRef.observeSingleEvent(of: .value) { (dataSnaphot) in
                    for drug in dataSnaphot.children.allObjects as? [DataSnapshot] ?? []{
                        if(pharmacyDrug.key == drug.key){
                           let drugModel = Drug(id: drug.key, dictionary: drug.value as? [String: Any] ?? [:])
                            
                            self.pharmacyDrugs.append(drugModel)
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func showPharmacyOnMap(_ mapView: MKMapView) {
        annotation.title = pharmacy.name
        guard let latitude = pharmacy.Latitude else { return }
        guard let longitude = pharmacy.longitude else { return }
        annotation.coordinate = CLLocationCoordinate2D(latitude: latitude , longitude: longitude )
        mapView.addAnnotation(annotation)
        
        let center = CLLocationCoordinate2D(latitude: latitude , longitude: longitude )
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: center, span: span)
        mapView.setRegion(region, animated: false)
    }
    
    override func loadView() {
        view = pharmacyDetailsView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchPharmacysDrugsData()
        fetchUsersDrugsData()
    }
    
    @objc func availablePillsClicked(){
        let loacVc = AvailablePillsController()
        loacVc.pills = pills
        navigationController?.present(loacVc, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateView()
        showNavigationBarHeader(headerImageView: nil)
        pharmacyDetailsView.delegate = self
        
        let availablePills = UIBarButtonItem(title: LocalizedString(key: "Available"), style: .done, target: self, action: #selector(availablePillsClicked))
        self.navigationItem.rightBarButtonItem  = availablePills
        
        navigationItem.title = pharmacy.name
        
        pharmacyDetailsView.mapView.frame = view.frame
        pharmacyDetailsView.mapView.delegate = self
        showActivityIndicator()
    }
}

extension PharmacyDetailViewController: PharmacyDetailViewDelegate{
    func didTapCallPhone() {
        guard let phone = pharmacy.phone else { return }
        phone.makeACall()
    }
}
