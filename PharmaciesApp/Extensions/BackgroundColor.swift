//
//  BackgroundColor.swift
//  PharmaciesApp
//
//  Created by a on 27/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    func BackgroundColor(){
        self.backgroundColor = .systemGray6
    }
}
