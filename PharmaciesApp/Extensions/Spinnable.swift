//
//  Spinnable.swift
//  PharmaciesApp
//
//  Created by a on 23/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
 
protocol Spinnable where Self: UIViewController {
    func showActivityIndicator()
    func hideActivityIndicator()
}
 
extension Spinnable {
    func showActivityIndicator() {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.style = .large
        activityIndicator.BackgroundColor()
        activityIndicator.color = .systemGreen
        activityIndicator.startAnimating()
        self.view.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { (make) in
            make.centerX.centerY.equalTo(view)
            make.top.bottom.equalTo(view)
            make.left.right.equalTo(view)
        }
    }

    func hideActivityIndicator() {
        for view in self.view.subviews {
            guard let activityIndicator = view as? UIActivityIndicatorView else {
                continue
            }
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
}
