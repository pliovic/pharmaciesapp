//
//  UITextField.swift
//  PharmaciesApp
//
//  Created by a on 30/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit.UITextField

extension UITextField {
    func validatedText(validationType: ValidatorType) throws -> String {
        let validator = VaildatorFactory.validatorFor(type: validationType)
        return try validator.validated(self.text!)
    }
}
