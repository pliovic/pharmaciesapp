//
//  NavigationBarrHeader.swift
//  PharmaciesApp
//
//  Created by a on 25/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

protocol NavigationBarHeader where Self: UIViewController {
    func showNavigationBarHeader(headerImageView: UIImageView?)
}

extension NavigationBarHeader {
    func showNavigationBarHeader(headerImageView: UIImageView?) {
        navigationController?.navigationBar.prefersLargeTitles = true
        
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.systemGreen]
        
        if(headerImageView != nil){
            guard let navigationBar = self.navigationController?.navigationBar else { return }
            navigationBar.addSubview(headerImageView!)
            headerImageView!.clipsToBounds = true
            headerImageView!.translatesAutoresizingMaskIntoConstraints = false
            headerImageView!.snp.makeConstraints { (make) in
                make.right.equalTo(CGFloat(-16))
                make.bottom.equalTo(CGFloat(-12))
                make.height.equalTo(CGFloat(40))
                make.width.equalTo(CGFloat(40))
            }
        }
    }
}
