//
//  DrugModel.swift
//  PharmaciesApp
//
//  Created by a on 24/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation

struct Drug: Equatable, Hashable {
    var id: String?
    var name: String?
    var dose: Double?
    var contain: Bool?
    
    init() {
        
    }
    
    init(id: String?, dictionary: [String: Any]) {
        self.id = id
        self.name = dictionary["name"] as? String
        self.dose = dictionary["dose"] as? Double
    }
}
