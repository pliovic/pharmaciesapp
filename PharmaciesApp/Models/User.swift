//
//  UserModel.swift
//  PharmaciesApp
//
//  Created by a on 24/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation

struct User {
    var uid: String?
    var name: String?
    var email: String?
    var photo: String?
    
    init() {
        
    }
    
    init(id: String?, dictionary: [String: Any]) {
        self.uid = id
        self.name = dictionary["name"] as? String
        self.email = dictionary["email"] as? String
        self.photo = dictionary["photo"] as? String
    }
}
