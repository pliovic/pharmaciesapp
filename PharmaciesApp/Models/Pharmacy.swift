//
//  PharmaciesModel.swift
//  PharmaciesApp
//
//  Created by a on 24/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation

struct Pharmacy {
    var id: String?
    var name: String?
    var address: String?
    var phone: String?
    var longitude: Double?
    var Latitude: Double?
    var distance: Double?
    
    init() {
        
    }
    
    init(id: String?, dictionary: [String: Any]) {
        self.id = id
        self.name = dictionary["name"] as? String
        self.address = dictionary["address"] as? String
        self.phone = dictionary["phone"] as? String
        self.Latitude = dictionary["latitude"] as? Double
        self.longitude = dictionary["longitude"] as? Double
    }
    
}
