//
//  Validators.swift
//  PharmaciesApp
//
//  Created by a on 30/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation

class ValidationError: Error {
    var message: String
    
    init(_ message: String) {
        self.message = message
    }
}

protocol ValidatorConvertible {
    func validated(_ value: String) throws -> String
}

enum ValidatorType {
    case requiredField(field: String)
    case doseField
}

enum VaildatorFactory {
    static func validatorFor(type: ValidatorType) -> ValidatorConvertible {
        switch type {
        case .requiredField(let fieldName): return RequiredFieldValidator(fieldName)
        case .doseField: return DoseValidator()
        }
    }
}

struct RequiredFieldValidator: ValidatorConvertible {
    private let fieldName: String
    
    init(_ field: String) {
        fieldName = field
    }
    
    func validated(_ value: String) throws -> String {
        guard !value.isEmpty else {
            throw ValidationError(LocalizedString(key: "requiredField") + fieldName)
        }
        return value
    }
}

class DoseValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        if(value.isEmpty){
            return value
        }
        guard (Double(value) != nil) else {throw ValidationError(LocalizedString(key: "doseMustBeANumber"))}
        
        return value
    }
}
