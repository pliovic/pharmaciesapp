//
//  Localized.swift
//  PharmaciesApp
//
//  Created by a on 31/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation

func LocalizedString(key: String) -> String{
    return NSLocalizedString(key, comment: "")
}
