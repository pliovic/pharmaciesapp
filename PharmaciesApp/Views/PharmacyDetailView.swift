//
//  PharmacyDetailView.swift
//  PharmaciesApp
//
//  Created by a on 25/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit
import MapKit

protocol PharmacyDetailViewDelegate: class {
    func didTapCallPhone()
}

class PharmacyDetailView: UIView {
    
    weak var delegate: PharmacyDetailViewDelegate?
    
    @objc func didTapCallPhone(){
        delegate?.didTapCallPhone()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createSubviews()
    }
    
    lazy var locationIconView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: "Pin")
        return image
    }()
    
    var addressLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name:"HelveticaNeue", size: 16.0)
        return label
    }()
    
    lazy var phoneIconView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: "Phone")
        return image
    }()
    
    private func phoneIconViewAutoLayout(){
        phoneIconView.snp.makeConstraints { (make) in
            make.top.equalTo(addressLabel.snp.bottom).offset(16)
            make.width.height.equalTo(20)
            make.leftMargin.equalTo(10)
        }
    }
    
    var phoneNumberButton: UIButton = {
        let button = UIButton(type: .system)
        button.addTarget(self, action: #selector(didTapCallPhone), for: .touchUpInside)
        return button
    }()
    
    private func phoneNumberButtonLayout(){
        phoneNumberButton.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(addressLabel.snp.bottom).offset(10)
            make.left.equalTo(phoneIconView.snp.left).offset(30)
        }
    }
    
    private func addressLabelutoLayout(){
        locationIconView.snp.makeConstraints { (make) in
            make.top.equalTo(self.safeAreaLayoutGuide.snp.top).offset(5)
            make.width.height.equalTo(20)
            make.leftMargin.equalTo(10)
        }
        
        addressLabel.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(self.safeAreaLayoutGuide.snp.top).offset(5)
            make.left.equalTo(self.locationIconView.snp.left).offset(30)
        }
    }
    
    var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.mapType = .standard
        return mapView
    }()
    
    private func mapViewAutoLayout(){
        mapView.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(phoneIconView.snp.bottom).offset(20)
            make.bottom.equalTo(self.snp.bottom)
            make.left.right.equalTo(self)
            make.right.equalTo(self)
        }
    }
    
    func createSubviews(){
        self.addSubview(mapView)
        self.addSubview(locationIconView)
        self.addSubview(addressLabel)
        self.addSubview(phoneIconView)
        self.addSubview(phoneNumberButton)
        self.BackgroundColor()
        mapViewAutoLayout()
        addressLabelutoLayout()
        phoneIconViewAutoLayout()
        phoneNumberButtonLayout()
    }
}
