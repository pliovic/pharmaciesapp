//
//  BrowsePharmaceisView.swift
//  PharmaciesApp
//
//  Created by a on 23/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import Foundation
import SnapKit

protocol BrowsePharmaceisDelegate: class {
    
}

class BrowsePharmaceisView: UIView{
    
    weak var delegate: BrowsePharmaceisDelegate?
    
    override init(frame: CGRect){
        super.init(frame: frame)
        createSubviews()
    }
    
    required init?(coder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
    }
    
    let tableView:UITableView={
        let tableView = UITableView()
        tableView.backgroundColor = .systemGray6
        return tableView;
    }()
    
    private func setTableView(){
        tableView.frame = self.frame
        
        tableView.register(BrowseItemTableViewCell.self, forCellReuseIdentifier: "Cell")
        self.addSubview(tableView)
    }
    
    private func tableViewAutoLayout(){
        tableView.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(self)
            make.left.right.equalTo(self)
        }
    }
    
    let mapImageView = UIImageView(image: UIImage(named: "Map"))
    
    func createSubviews(){
        setTableView()
        tableViewAutoLayout()
    }
}
