//
//  PharmaciesOnMapView.swift
//  PharmaciesApp
//
//  Created by a on 25/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit
import MapKit

class PharmaciesOnMapView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createSubviews()
    }
    
    var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.mapType = .standard
        return mapView
    }()
    
    private func mapViewAutoLayout(){
        mapView.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(self.safeAreaLayoutGuide.snp.top)
            make.bottom.equalTo(self.snp.bottom)
            make.left.right.equalTo(self)
            make.right.equalTo(self)
        }
    }
    
    func createSubviews(){
        self.addSubview(mapView)
        
        self.BackgroundColor()
        mapViewAutoLayout()
    }
}
