//
//  NewDrugView.swift
//  PharmaciesApp
//
//  Created by a on 25/03/2020.
//  Copyright © 2020 a. All rights reserved.
//

import UIKit

protocol NewDrugViewDelegate: class {
    func didTapSubmitNewUsersDrugButton()
}

class NewDrugView: UIView {
    
    weak var delegate: NewDrugViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createSubviews()
    }
    
    static private func txtFieldBase(txtField: UITextField){
        txtField.textColor = .systemGray2
        txtField.autocapitalizationType = UITextAutocapitalizationType.none
        txtField.backgroundColor = .systemGray5
        txtField.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0)
    }
    
     var drugNameTxtField:UITextField = {
        let txtField = UITextField()
        txtFieldBase(txtField: txtField)
        txtField.placeholder = LocalizedString(key: "drugName")
        return txtField
    }()
    
    let drugDoseTxtField:UITextField = {
        let txtField = UITextField()
        txtField.keyboardType = UIKeyboardType.decimalPad
        txtFieldBase(txtField: txtField)
        txtField.placeholder = LocalizedString(key: "dose")
        return txtField
    }()
    
     let submitButton:UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .systemGreen
        button.setTitle(LocalizedString(key: "submit"), for: .normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(didTapSubmitNewUsersDrugButton), for: .touchUpInside)
        return button
    }()
    
    @objc func didTapSubmitNewUsersDrugButton(){
        delegate?.didTapSubmitNewUsersDrugButton()
    }
    
    private func drugNameTxtFieldAutoLayout(){
        drugNameTxtField.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(self.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(self).offset(50)
            make.right.equalTo(self.snp.right).offset(-50)
            make.height.equalTo(45)
        }
    }
    
    private func drugDoseTxtFieldAutoLayout(){
        drugDoseTxtField.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(drugNameTxtField.snp.bottom).offset(20)
            make.left.equalTo(self.snp.left).offset(50)
            make.right.equalTo(self.snp.right).offset(-50)
            make.height.equalTo(45)
        }
    }
    
    private func submitButtonAutoLayout(){
        submitButton.snp.makeConstraints{(make) -> Void in
            make.top.equalTo(drugDoseTxtField.snp.bottom).offset(40)
            make.left.equalTo(self.snp.left).offset(50)
            make.right.equalTo(self.snp.right).offset(-50)
            make.height.equalTo(50)
        }
    }
    
    private func setupConstraints(){
        drugNameTxtFieldAutoLayout()
        drugDoseTxtFieldAutoLayout()
        submitButtonAutoLayout()
    }
    
    func createSubviews(){
        self.BackgroundColor()
        self.addSubview(drugNameTxtField)
        self.addSubview(drugDoseTxtField)
        self.addSubview(submitButton)
        
        setupConstraints()
    }
}
